# 4DMRI_moving_lung_meshes

**Moving lung meshes extracted from 4DMRIs**

----------------------------------------------------------------------------------------------------

Please cite the following publications when using the data:

Duetschler, A., Bauman, G., Bieri, O., Cattin, P.C., Ehrbar, S., Engin-Deniz, G., Giger, A., Josipovic, M., Jud, C., Krieger, M., Nguyen, D., Persson, G.F., Salomir, R., Weber, D.C., Lomax, A.J. and Zhang, Y. (2022), *Synthetic 4DCT(MRI) lung phantom generation for 4D radiotherapy and image guidance investigations*. Med. Phys.. [https://doi.org/10.1002/mp.15591](https://doi.org/10.1002/mp.15591)

Duetschler A, Krieger M, Giger A, et al. (2021). 4DMRI moving lung meshes. Zenodo. [doi:10.5281/zenodo.5016294](https://doi.org/10.5281/zenodo.5016294)

In case of questions, please contact Alisha Dütschler (alisha.duetschler@psi.ch).

----------------------------------------------------------------------------------------------------

The moving lung meshes can for example be used to generate synthetic 4DCT(MRI)s. The code for generating 4DCT(MRI)s using a reference CT and the moving lung mesh data can be found [here](https://gitlab.psi.ch/duetschler_a/4DCT-MRI). 

----------------------------------------------------------------------------------------------------
**Clone repository**

You can clone the repository from Gitlab. In the terminal navigate to the desired location and clone the repository

    git clone https://gitlab.psi.ch/duetschler_a/4DMRI_moving_lung_meshes.git
    
----------------------------------------------------------------------------------------------------
### 4DMRI acquisition and mesh extraction

4DMRIs have been acquired for five volunteers. Each of the 4DMRIs was acquired over 11 minutes with a **framerate of 2.25 Hz**. The 4DMRIs were all reconstructed using a 'motion aware' method (Jud *et al.*, 2018). An end exhale (EE) reference phase was used for the deformation vector field derivation (Sandkühler *et al.*, 2018). More details can be found in Giger *et al.* (2020).

Giger A, Krieger M, Jud C. et al. Liver-ultrasound based motion modelling to estimate 4D dose distributions for lung tumours in scanned proton therapy. *Phys Med Biol.* 2020;65(23):235050. Published 2020 Dec 22. doi:10.1088/1361-6560/abaa26

Jud C, Nguyen D, Sandkühler R, Giger A, Bieri O, Cattin PC. Motion aware MR imaging via spatial code correspondence. *In: Lecture Notes in Computer Science (Including Subseries Lecture Notes in Artificial Intelligence and Lecture Notes in Bioinformatics).* Vol 11070 LNCS. Springer Verlag; 2018:198-205. doi:10.1007/978-3-030-00928-1_23
    
Sandkühler R, Jud C, Amdermatt S, Cattin Pc. AirLab: Autograd Image Registration Laboratory. Published online June 26, 2018, Accessed June 3, 2020. http://arxiv.org/abs/1806.09907

Both lung halves were manually segmented on the chosen reference states and converted to surface meshes using [vtk](https://vtk.org/)[1]. A regular grid was then inserted inside the surface mesh. The meshes are then animated using the deformation vectors extracted at the mesh point locations.

### 4DMRI moving lung mesh data 

The data of five volunteers (MRI1-MRI5) is included. The data is separated into mesh data for an end exhalation (EE) and an end inhalation (EI) reference phase.

Folder structure: 
The reference phase lung masks (lungs_left.mha and lungs_right.mha) can be found in the structures folder for each MRI. These masks were then smoothed and downsampled (lungs_left_smooth.mha and lungs_right_smooth.mha) and the resulting masks were used to obtain the reference lung surface meshes (lungs_left.vtk and lungs_right.vtk). 
The MovingMRmesh folder again contains the reference surface lungs meshes (lungs_left_surface.vkt and lungs_right_surface.vkt), a regular grid of points inside each lung half (lungs_left_grid.vkt and lungs_right_grid.vkt) and both combined (lungs_left_surface_and_grid.vtk and lungs_right_surface_and_grid.vtk). The subfolders Left and Right contain the moving MR meshes for each 4DMRI state (state_0000.vkt - state_1499.vtk). 

The motion patterns are visualized in Figure 1 and information about breathing states and lung volumes can be found in Table 1 and 2. 

![4DMRI_motion](4DMRI_motion.jpg)
***Figure 1.*** Visualization of mesh motion. For each MRI a point in the dome of each lung half was selected (a) and the superior-inferior (SI) motion analyzed. A boxplot of the amplitudes and the periods over all breathing cycles is shown in (b) and (c). The SI motion for the selected points in the dome of each lung half is plotted over time (d-h). Some selected motion patterns are indicated by grey shaded areas.


***Table 1.*** Table listing selected end exhalation (EE) and end inhalation (EI) reference states and their lung volumes.

|  | EE reference state | EI reference state | lung volume L EE [ml]| lung volume R EE [ml]| lung volume L EI [ml]| lung volume R EI [ml]|
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| **MRI1** | 24 | 28 | 1787 | 1980 | 1808 | 2197 |
| **MRI2** | 25 | 28 | 1252 | 1658 | 1474 | 1912 |
| **MRI3** | 19 | 22 | 1735 | 2162 | 1889 | 2342 |
| **MRI4** | 13 | 18 | 1291 | 1446 | 1612 | 1795 |
| **MRI5** | 17 | 21 | 1885 | 2153 | 2134 | 2440 |

***Table 2.*** Start and end states of selected motion patterns (marked by grey shaded areas in Figure 1).

|  | start state motion pattern 1 | end state motion pattern 1 | start state motion pattern 2 | end state motion pattern 2 |
| ------ | ------ | ------ | ------ | ------ | 
| **MRI1** | 24 | 77 | 284 | 357 | 
| **MRI2** | 25 | 69 | 1177 | 1295 | 
| **MRI3** | 19 | 64 | - | - |
| **MRI4** | 13 | 231 | - | - | 
| **MRI5** | 17 | 63 | 595 | 691 | 

## References

[1] Schroeder W, Martin K, Lorensen B. *The Visualization Toolkit.* 4th ed. Kitaware; 2006
